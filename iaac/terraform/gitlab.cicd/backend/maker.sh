#!/bin/bash

echo "--make file key for s3account"
yc iam key create --service-account-name s3account --output s3bucket.key
echo "--copy s3bucket.key to /tmp/ for later use"
cp s3bucket.key /tmp/
echo "--build k8cluster"
chmod +x builderk8.sh; cp builderk8.sh ../k8stock; cd ../k8stock; ./builderk8.sh

