//-----------------------------------------------------------------------
// variables
// ----------------------------------------------------------------------
variable "stockvars" {
  type = object({
    folder_id    = string
    zone         = string

  })
  default = {
    folder_id    = "b1g6jkfnul7b3vp4jegm"
    zone         = "ru-central1-a"
  }
}
//-----------------------------------------------------------------------
// prepare with scripts
// ----------------------------------------------------------------------
variable "file_watch" {
  type = string
  description = "script file location"
  default = "maker.sh"
}
variable "file_location" {
  type = string
  description = "script file location"
  default = "maker.sh"
}
# - build k8cluster runer -
resource "null_resource" "exportk8" {
  provisioner "local-exec" {
    command = "echo  'terraform init -backend-config=\"access_key=${yandex_storage_bucket.s3tfb.access_key}\" -backend-config=\"secret_key=${nonsensitive(yandex_iam_service_account_static_access_key.static-access-key.secret_key)}\";terraform apply -auto-approve' > builderk8.sh"
  }
}
# - run k8cluster runer -
resource "null_resource" "run_script" {
  # - run if get changes files .tf
  triggers = {
    script_hash = filemd5(var.file_watch)
  }
  # - run script when the configuration file has changed
  provisioner "local-exec"  {
    command = "bash ./${var.file_location}"
  }
}
