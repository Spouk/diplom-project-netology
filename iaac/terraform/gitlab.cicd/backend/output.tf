//-----------------------------------------------------------------------
// output s3
// ----------------------------------------------------------------------
output "s3backet" {
  value = format("\taccesskey: %v\n\tbucketid: %v\n\tsecretkey: %v\n",
    yandex_storage_bucket.s3tfb.access_key,
    yandex_storage_bucket.s3tfb.id,
    nonsensitive(yandex_iam_service_account_static_access_key.static-access-key.secret_key)
  )
}
#- folder_id
output "folder_id" {
  value = yandex_storage_bucket.s3tfb.folder_id
}
#- s3 bucket private key
output "s3pkey" {
  value = nonsensitive(yandex_iam_service_account_static_access_key.static-access-key.secret_key)
}
#- s3 bucket access key
output "s3akey" {
  value = yandex_storage_bucket.s3tfb.access_key
}
#- s3 id
output "s3id" {
  value = yandex_storage_bucket.s3tfb.id
}
# - s3account
output "said" {
  value = yandex_iam_service_account.s3account.id
}
