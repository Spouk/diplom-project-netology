//-----------------------------------------------------------------------
// terraform provider
// ----------------------------------------------------------------------
terraform {
  // -- adding after first apply for uppp
  backend "s3" {
    endpoints = {
      s3 = "https://storage.yandexcloud.net"
    }
    bucket         = "s3tfb"
    region         = "ru-central1"
    key            = "backend/terraform.tfstate"

        access_key                  = "YCAJETwK_FTa0IxFGsIbbEpeV"
        secret_key                  = "YCNwu19Ug0uswOW9g-GqgfBVYj6LOBHzW_jR5D1B"
        skip_region_validation      = true
        skip_credentials_validation = true
        skip_requesting_account_id  = true # необходимая опция terraform для версии 1.6.1 и старше.
        skip_s3_checksum            = true # необходимая опция при описании бэкенда для terraform версии 1.6.3 и старше.

  }
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-a"
}
//-----------------------------------------------------------------------
// service account for manipulation
// ----------------------------------------------------------------------
resource "yandex_iam_service_account" "s3account" {
  name        = "s3account"
  description = "service account for iaac diplom devops netology"
  folder_id   = var.stockvars.folder_id
}
resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  folder_id = var.stockvars.folder_id
  role      = "editor"
  members   = [
    "serviceAccount:${yandex_iam_service_account.s3account.id}",
  ]
  depends_on = [
    yandex_iam_service_account.s3account,
  ]
}
resource "yandex_iam_service_account_static_access_key" "static-access-key" {
  service_account_id = yandex_iam_service_account.s3account.id
  depends_on         = [
    yandex_iam_service_account.s3account,
  ]
}
//-----------------------------------------------------------------------
// s3 symetric key for crypting data in data object = s3 backet
// ----------------------------------------------------------------------
resource "yandex_kms_symmetric_key" "key-a" {
  name              = "s3terraform-symetric-key"
  description       = "s3terraform-symetric-key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h"
}
//-----------------------------------------------------------------------
// s3 object storage
// ----------------------------------------------------------------------
resource "yandex_storage_bucket" "s3tfb" {
  access_key            = yandex_iam_service_account_static_access_key.static-access-key.access_key
  secret_key            = yandex_iam_service_account_static_access_key.static-access-key.secret_key
  bucket                = "s3tfb"
  folder_id             = var.stockvars.folder_id
  default_storage_class = "COLD"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = yandex_kms_symmetric_key.key-a.id
        sse_algorithm     = "aws:kms"
      }
    }
  }
  tags = {
    name        = "s3bucket"
    description = "bucket for save terraform state file(s)"
  }
}
