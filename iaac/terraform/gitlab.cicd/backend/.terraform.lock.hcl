# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/null" {
  version = "3.2.2"
  hashes = [
    "h1:zT1ZbegaAYHwQa+QwIFugArWikRJI9dqohj8xb0GY88=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.111.0"
  hashes = [
    "h1:XLrWiW5dJzxCPcXk10waWaHJNq97bhXFM7Km8Bb1I6I=",
  ]
}
