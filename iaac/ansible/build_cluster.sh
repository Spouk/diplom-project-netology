#!/bin/bash

echo "--install k8 cluster with kubespray"
source venv/bin/activate
ansible-playbook -i host.ini  --private-key=~/.ssh/remoteuser -u ubuntu -b -v kubespray/cluster.yml --become --become-user=root   -e upgrade_cluster_setup=true
deactivate

echo "--change correct kube config remote ip "


