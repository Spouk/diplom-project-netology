// this file has the baseline default parameters
{
  components: {
  appdock: {
    name: "appdock",
    image: "cyberspouk/net-d-nginx:latest",
    replicas: 3,
    containerPort: 80,
    servicePort: 80,
    nodeSelector: {},
    tolerations: {},
    ingressClass: "nginx",
    domain: "app.local",
    domainextern: "app.spouk.ru",
    typeService: "ClusterIP",
    protocolService: "TCP",
  }
  },
}
