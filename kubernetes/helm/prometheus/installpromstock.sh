#!/bin/bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
#cp prometheus/values.yaml prometheus-values.yaml
helm upgrade --install --create-namespace --values prometheus-values.yaml prometheus -n monitoring prometheus-community/prometheus
