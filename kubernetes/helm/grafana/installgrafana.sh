#!/bin/bash
#tar zxf grafana-7.3.3.tgz
helm repo add grafana https://grafana.github.io/helm-charts
helm repo list
#cp grafana/values.yaml grafana-values.yaml
helm upgrade --install --create-namespace --values grafana-values.yaml grafana -n monitoring grafana/grafana
